<?php
   include("config.php");
   session_start();
   error_reporting(0);
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form

      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']);

      $sql = "SELECT id FROM members WHERE username = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $active = $row['active'];
      $count = mysqli_num_rows($result);

      // If result matched $myusername and $mypassword, table row must be 1 row

      if($count == 1) {
         //session_register("myusername");
         $_SESSION['login_user'] = $myusername;

         header("location: home.php");
      }else {
         $error = "Pogrešno korisničko ime ili lozinka!";
      }
   }
?>
<!DOCTYPE html>
<html lang="rs">
<head>
  <title>Radio S</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2 align="center">Radio S Emitovanje</h2>
  <form class="form-horizontal" align="center" action="" method="post">
    <div class="form-group">
      <label class="control-label col-sm-4" for="username">Korisničko ime:</label>
      <div class="col-sm-4">
        <input type="text" class="form-control" name="username" id="username" placeholder="Unesite korisničko ime">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-4" for="pwd">Lozinka:</label>
      <div class="col-sm-4">
        <input type="password" class="form-control" id="pwd" name="password" placeholder="Unesite lozinku">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary" value = " Submit ">Prijava</button>
      </div>
    </div>
	<?php echo $error; ?>
  </form>
</div>
</body>
</html>
