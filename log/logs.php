<html lang="rs">
<head>
		<meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Radio S </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="../css/jquery-ui.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>

</head>
<body>
<?php
include('../menu.php');
?>
<div class="container" style="margin-top:50px;">
	<form action="excel.php" method="POST">
		<p>Od: <input type="text" name="date" id="datepicker"></p>
		<p>Do: <input type="text" name="date2" id="datepicker2"></p>
		<input type="submit" class="btn btn-success" value="Submit" />
	</form>
</div>


<script src="../js/jquery-1.12.4.js"></script>
<script src="../js/jquery-ui.js"></script>
<script>
	$( function() {
		$( "#datepicker" ).datepicker();
		$( "#datepicker2" ).datepicker();
	} );
</script>

<?php
include('../footer.php');
?>
</body>
</html>
