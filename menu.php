<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"></button>
      <a class="navbar-brand" href="home.php">Radio S</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
    		<li><a href="comp.php">Autori</a></li>
    		<li><a href="pesme.php">Pesme</a></li>
        <li><a href="proba.php">Compare</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Odjava</a></li>
      </ul>
    </div>
  </div>
</nav>
